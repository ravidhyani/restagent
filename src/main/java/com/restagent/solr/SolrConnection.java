package com.restagent.solr;

import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;


public interface SolrConnection {
	
//	public String addSolrRattingDoc(ReviewRatting reviewRatting, String solrUrl);
	
//	public String addhelpFullReview(ReviewRatting reviewRatting, String solrUrl);
	
//	public String addLikeOnReview(ReviewRatting reviewRatting, String solrUrl);
	
	public QueryResponse searchDocument(String solrUrl, String query);
	
	public UpdateResponse deleteByQuery(String solrUrl, String query);
	
	public UpdateResponse addDocument(String solrUrl,SolrInputDocument document);
	
	public UpdateResponse updateDocument(String solrUrl,SolrInputDocument document);
	
	public QueryResponse advanceSerach(String solrUrl, Map<String, String> searchCriteria);
}
