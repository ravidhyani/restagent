package com.restagent.solr;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.stereotype.Component;

import com.restagent.util.Utility;


@Component
public class SolrConnectionImpl implements SolrConnection {

	/*@Override
	public String addSolrRattingDoc(ReviewRatting reviewRatting, String solrUrl) {
	
		HttpSolrClient solr=Utility.getSolrClient(solrUrl);
		SolrInputDocument document = new SolrInputDocument();
		document.addField("reviewId", reviewRatting.getId());
		document.addField("contentId", reviewRatting.getReviewContentId());
		document.addField("userId", reviewRatting.getReviewUserId());
		document.addField("ratting", reviewRatting.getReviewRatting());
		try {
			solr.add(document);
			solr.commit();
		} catch (SolrServerException | IOException e) {
			e.printStackTrace();
			return "failed";
		}		
		return "sucess";
	}*/
	
	@Override
	public QueryResponse searchDocument(String solrUrl, String query) {
		System.out.println("iam here");
		QueryResponse response=null;
		System.out.println("iam here 1");
		HttpSolrClient solr=Utility.getSolrClient(solrUrl);
		solr.setParser(new XMLResponseParser());
		
		final Map<String, String> queryParamMap = new HashMap<String, String>();
		queryParamMap.put("q", query);
		MapSolrParams queryParams = new MapSolrParams(queryParamMap);
		
		try {
			response=solr.query(queryParams);
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public UpdateResponse deleteByQuery(String solrUrl, String query) {
		HttpSolrClient solr=Utility.getSolrClient(solrUrl);
		solr.setParser(new XMLResponseParser());
		UpdateResponse response = null;
		try {
			response = solr.deleteByQuery(query);
			solr.commit();
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return response;
	}

	@Override
	public UpdateResponse addDocument(String solrUrl, SolrInputDocument document) {
		UpdateResponse updateResponse=null;
		HttpSolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
		solr.setParser(new XMLResponseParser());
	
		try {
			solr.add(document);
			updateResponse=solr.commit();
			System.out.println("Inside SolrRattingImpl end");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		return updateResponse;
	}
	@Override
	public UpdateResponse updateDocument(String solrUrl,SolrInputDocument document) {
	      SolrClient Solr = new HttpSolrClient.Builder(solrUrl).build();   
	      UpdateRequest updateRequest = new UpdateRequest();  
	      updateRequest.setAction( UpdateRequest.ACTION.COMMIT, false, false);    	      
	      UpdateResponse rsp = null;
		try {
				updateRequest.add( document);  
			  rsp = updateRequest.process(Solr); 
			  System.out.println("record Updated");
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during user update: " + e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Error during user update: " + e.getMessage());
		} 
	      return rsp;
	}

	@Override
	public QueryResponse advanceSerach(String solrUrl,Map<String, String> searchCriteria) {
		
		QueryResponse response=null;
		HttpSolrClient solr=Utility.getSolrClient(solrUrl);
		solr.setParser(new XMLResponseParser());
		
		
	//	final Map<String, String> queryParamMap = new HashMap<String, String>();
		// queryParamMap.put("q", query);
		// queryParamMap.put("fl", "id, name");
		MapSolrParams queryParams = new MapSolrParams(searchCriteria);
		
		try {
			response=solr.query(queryParams);
		} catch (SolrServerException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}
}