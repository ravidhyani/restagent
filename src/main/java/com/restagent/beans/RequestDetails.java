package com.restagent.beans;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public class RequestDetails {

	public RequestDetails() {
		// TODO Auto-generated constructor stub
	}
	private String solrURL;
	private String methodName;
	private HttpMethod requestType;
	private MediaType mediaType;
	

	public RequestDetails(String solrURL, String methodName, HttpMethod requestType, MediaType mediaType) {
		super();
		this.solrURL = solrURL;
		this.methodName = methodName;
		this.requestType = requestType;
		this.mediaType = mediaType;
	}

	public String getSolrURL() {
		return solrURL;
	}
	public void setSolrURL(String solrURL) {
		this.solrURL = solrURL;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public HttpMethod getRequestType() {
		return requestType;
	}
	public void setRequestType(HttpMethod requestType) {
		this.requestType = requestType;
	}
	public MediaType getMediaType() {
		return mediaType;
	}
	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	

}
