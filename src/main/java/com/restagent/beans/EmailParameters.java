package com.restagent.beans;

public class EmailParameters {

	private String emailTo;
	private String emailSubject;
	private String emailBody;
	private String attachmentName;
	private String attachmentPath;
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public String getEmailSubject() {
		return emailSubject;
	}
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	public String getEmailBody() {
		return emailBody;
	}
	public void setEmailBody(String emailBody) {
		this.emailBody = emailBody;
	}
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getAttachmentPath() {
		return attachmentPath;
	}
	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
	public EmailParameters(String emailTo, String emailSubject, String emailBody, String attachmentName,
			String attachmentPath) {
		super();
		this.emailTo = emailTo;
		this.emailSubject = emailSubject;
		this.emailBody = emailBody;
		this.attachmentName = attachmentName;
		this.attachmentPath = attachmentPath;
	}
	public EmailParameters() {
		super();
	}
	
	
}
