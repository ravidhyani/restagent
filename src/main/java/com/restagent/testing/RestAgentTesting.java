package com.restagent.testing;

import java.util.Date;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.ResponseErrorHandler;

import com.restagent.beans.EmailParameters;
import com.restagent.beans.RequestDetails;
import com.restagent.controller.Communication;
import com.restagent.controller.CommunicationImpl;
import com.restagent.util.SolrUrls;

@SpringBootApplication
@ComponentScan(basePackages = "com.restagent") //pass pkg name here
public class RestAgentTesting  implements CommandLineRunner  {

	@Autowired
	private JavaMailSender javaMailSender;

	 
	public RestAgentTesting() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		SpringApplication.run(RestAgentTesting.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		ResponseErrorHandler responseHandler = new ResponseErrorHandler() {
			
			public boolean hasError(ClientHttpResponse response) throws IOException {

				if (response.getStatusCode() != HttpStatus.OK) {
					System.out.println("Error: " + response.getStatusText() + response.getRawStatusCode());
				}
				return response.getStatusCode() == HttpStatus.OK ? false : true;
			}
			
			public void handleError(ClientHttpResponse response) throws IOException {
				// TODO Auto-generated method stub
				System.out.println("Errorrr: " + response.getStatusText() + response.getRawStatusCode());
			}
		}; 

		try {

			//String responseBody = new CommunicationImpl<String, String>().processRequest(new RequestDetails("http://localhost:8085/test", HttpMethod.GET), " ", responseHandler, String.class);
			//String responseBody = new CommunicationImpl<String, String>().processRequest(new RequestDetails("http://localhost:8085/ratingAnalysis", HttpMethod.POST), "Data posted on " + new Date(), responseHandler, String.class);
			String data = "{\"text\":\"hello All....  Data posted on " + new Date() + "\"}";
			Communication<String, String> comm = new CommunicationImpl<String, String>(javaMailSender) ;
		//	String responseBody = comm.processRequest(new RequestDetails(SolrUrls.protocolUrl ,"addReview1", HttpMethod.POST, MediaType.APPLICATION_JSON), data, responseHandler, String.class);
		//	System.out.println("Response: "+ responseBody);
			EmailParameters emailParams = new EmailParameters("simran03@gmail.com", "Testing email service from Restagent", "<h2>This email has been sent on " + new Date() + "</h2>", "my_photo.png", "image1.jpeg");
			System.out.println("Response from sendEmail: " + comm.sendEmail(emailParams));	 

		} catch (Exception e) {
 			e.printStackTrace();
		}
	}
 
}
