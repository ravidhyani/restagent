package com.restagent.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;

public class Utility {	
	
	public static String getUniqueId() {
		return UUID.randomUUID().toString();
	}

	public static String getCurrentDateAndTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date); //2020/03/26 12:08:43
	}
	
	
	public static HttpSolrClient getSolrClient(String solrUrl) {
		HttpSolrClient solr = new HttpSolrClient.Builder(solrUrl).build();
		solr.setParser(new XMLResponseParser());
		return solr;
	}
}