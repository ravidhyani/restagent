package com.restagent.service;

import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.restagent.solr.SolrConnection;
import com.restagent.util.Utility;


@Component
public class CommonDocumentServiceImpl implements CommonDocumentService{
	
	@Autowired
	SolrConnection solrConnection;

	public String addDocument(Map<String, Object> payload,String url) {
		String ID=Utility.getUniqueId();
		SolrInputDocument document = new SolrInputDocument();
		document.addField("ID",ID);
	 	solrConnection.addDocument(url, this.createDoc(payload, document));
		return ID;
	}
	
	@Override
	public String updateDocument(Map<String, Object> payload, String url) {
		SolrInputDocument document = new SolrInputDocument();
		 solrConnection.updateDocument(url, this.createDoc(payload, document));
		return "sucess";
	}
	
	public QueryResponse advanceSearchDocument(Map<String, String> searchCriteria,String url) {
		
		return solrConnection.advanceSerach(url, searchCriteria);
	}
	
	public SolrInputDocument createDoc(Map<String, Object> payload, SolrInputDocument document) {
		payload.forEach((k,v) ->{
	 		document.addField(k, v);
	 	});
		return document;
	}

	@Override
	public QueryResponse SearchByQuery(String query, String url) {
		return solrConnection.searchDocument(url, query);
	}
}