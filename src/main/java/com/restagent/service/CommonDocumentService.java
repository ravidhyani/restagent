package com.restagent.service;

import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;

public interface CommonDocumentService {
	
	public String addDocument(Map<String, Object> payload , String url);
	
	public String updateDocument(Map<String, Object> payload , String url);

	public QueryResponse advanceSearchDocument(Map<String, String> searchCriteria , String url);
	
	public QueryResponse SearchByQuery(String query , String url);
}