package com.restagent.controller;

import java.io.IOException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseErrorHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.restagent.beans.EmailParameters;
import com.restagent.beans.RequestDetails;
import com.restagent.solr.SolrConnection;
import com.restagent.solr.SolrConnectionImpl;
import com.restagent.util.ResponseMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class CommunicationImpl<T, V> implements Communication<T, V> {

	private RestTemplate restTemplate = new RestTemplate();
	@Autowired
	SolrConnection solrConnection;
	
    private JavaMailSender javaMailSender;
	
    
	public CommunicationImpl() {
		super();
	}

	public CommunicationImpl(JavaMailSender javaMailSender) {
		super();
		this.javaMailSender = javaMailSender;
	}

	@Override
	public V processRequest(RequestDetails requestDetails, T data, ResponseErrorHandler errorHandler,
			Class<V> genericClass) throws ResourceAccessException, Exception {
		// TODO Auto-generated method stub
		HttpMethod httpMethod = requestDetails.getRequestType(); 
		ModelMap model=new ModelMap();
		String query="methodName:"+ requestDetails.getMethodName();
		System.out.println("iam here 0");
		solrConnection = new SolrConnectionImpl();
		QueryResponse qr = solrConnection.searchDocument(requestDetails.getSolrURL(), query);
		 SolrDocumentList solrDocList = qr.getResults();
		 System.out.println("iam here 2");
		if(solrDocList != null && solrDocList.getNumFound()>0) { 
			System.out.println("Method Found");
			//model.addAttribute("Message", new ResponseMessage("review already added", "AlreadyExist"));
			
			SolrDocument solrDoc = solrDocList.get(0);
			String methodName = (String) solrDoc.getFieldValue("methodName");
			String protocol = (String) solrDoc.getFieldValue("protocol");
			String address = (String) solrDoc.getFieldValue("address");
			String portNo = (String) solrDoc.getFieldValue("portNo");
			String path = (String) solrDoc.getFieldValue("path");
			String absolURL = (String) solrDoc.getFieldValue("absolURL");
			
			System.out.println("---methodName: " + methodName);
			//absolURL = "https://hooks.slack.com/services/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";
			if(absolURL == null || absolURL.isEmpty()) {				
				absolURL = protocol + "://" + address + ":" + portNo + "/" + path ;
			} 
			System.out.println("---Absolute URL: " + absolURL);
			restTemplate.setErrorHandler(errorHandler);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(requestDetails.getMediaType()); 
			HttpEntity<T> entity = new HttpEntity<T>(data, headers);
			//V response = restTemplate.postForObject(absolURL, data, genericClass);
			ResponseEntity<V> response = restTemplate.exchange(absolURL, httpMethod, entity, genericClass);
			System.out.println("================ " + response.getBody());
			return response.getBody();

		} else {
			model.addAttribute("Message", new ResponseMessage("No method detail found", "NotFound"));
		}		
		return null;
	}

	public String postSlackMessage(String hookUrl, String hookMessage) {

		try {
			com.restagent.beans.SlackMessage message = new com.restagent.beans.SlackMessage();
			System.out.println("------Slack Message --------- " + hookMessage);
			if (hookMessage != null) {
				CloseableHttpClient client = HttpClients.createDefault();
				HttpPost httpPost = new HttpPost(hookUrl);
				// "https://hooks.slack.com/services/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"

				message.setText(hookMessage);
				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writeValueAsString(message);

				StringEntity entity = new StringEntity(json);
				httpPost.setEntity(entity);
				httpPost.setHeader("Accept", "application/json");
				httpPost.setHeader("Content-type", "application/json");

				client.execute(httpPost);
				client.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String postCronMessage(String cronUrl, String cronMessage) {
		// http://0940f7b7.ngrok.io/ratingAnalysis
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.postForObject(cronUrl, cronMessage, String.class);

		System.out.println("back : " + result);
		return result;
	}

	public String sendEmail(EmailParameters emailParams) throws MessagingException, IOException {
		
        MimeMessage msg = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
		
        helper.setTo(emailParams.getEmailTo());
        helper.setSubject(emailParams.getEmailSubject());
        helper.setText(emailParams.getEmailBody(), true);

		// hard coded a file path
        //FileSystemResource file = new FileSystemResource(new File("path/android.png"));

        if(emailParams.getAttachmentName() != null && emailParams.getAttachmentPath()!=null) {
        	helper.addAttachment(emailParams.getAttachmentName(), new ClassPathResource(emailParams.getAttachmentPath()));
        }

        javaMailSender.send(msg);
        return "Email sent successfully...";
	}

	public JavaMailSender getJavaMailSender() {
		return javaMailSender;
	}

	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

	
	
	
	/*@PostConstruct
    public void invokeSlackWebhook() {
        RestTemplate restTemplate = new RestTemplate();
        RichMessage richMessage = new RichMessage("Just to test Slack's incoming webhooks.");
        // set attachments
        Attachment[] attachments = new Attachment[1];
        attachments[0] = new Attachment();
        attachments[0].setText("Some data relevant to your users.");
        richMessage.setAttachments(attachments);

        // For debugging purpose only
        try {
            logger.debug("Reply (RichMessage): {}", new ObjectMapper().writeValueAsString(richMessage));
        } catch (JsonProcessingException e) {
            logger.debug("Error parsing RichMessage: ", e);
        }

        // Always remember to send the encoded message to Slack
        try {
            restTemplate.postForEntity(slackIncomingWebhookUrl, richMessage.encodedMessage(), String.class);
        } catch (RestClientException e) {
            logger.error("Error posting to Slack Incoming Webhook: ", e);
        }*/
}
