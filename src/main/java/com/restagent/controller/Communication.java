package com.restagent.controller;

import java.io.IOException;

import javax.mail.MessagingException;

import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.ResponseErrorHandler;

import com.restagent.beans.EmailParameters;
import com.restagent.beans.RequestDetails;

public interface Communication<T, V> {

	public V processRequest (RequestDetails requestDetails, T data, ResponseErrorHandler errorHandler, Class<V> genericClass)  throws ResourceAccessException, Exception ;
	
	String postSlackMessage(String hookUrl, String hookMessage);

	String postCronMessage(String cronUrl, String cronMessage);
	
	String sendEmail(EmailParameters emailParams) throws MessagingException, IOException;
}
